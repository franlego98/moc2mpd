#ifndef NCURSES_UTILS_HPP
#define NCURSES_UTILS_HPP

#include <curses.h>
#include <string.h>
#include <stdlib.h>
#include "lists.hpp"

typedef struct progress_bar {
	char* name;
	int len;
	int value;
	int y,x;
	WINDOW* win;
} progress_bar;

progress_bar *progress_bar_new(WINDOW *w,int y,int x,int len,char* name);
void progress_bar_draw(progress_bar *bar);
void progress_bar_set_value(progress_bar *bar,int value);

typedef struct nlist {
	WINDOW* win;
	char* title;
	int selected;
	linked_list *items;
} nlist;

nlist *nlist_new(int y,int x,int height,int width,char *title);
void add_items_list(nlist* l,linked_list *items);
void nlist_draw(nlist *l);

#endif
