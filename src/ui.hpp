#ifndef UI_HPP
#define UI_HPP

#include "ncurses_utils.hpp"
#include <mpd/client.h>

struct ui_state {
	int need_queue_update;
	int random;
	int consume;
	int repeat;
	int last_queue_version;
	int state;
};

void refresh_queue_data();
void refresh_ui();
void refresh_ui_data();
void init_ui();

#endif
