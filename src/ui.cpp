#include "ui.hpp"

extern mpd_connection *conn_ui;
extern mpd_connection *conn_keyboard;

char song_info_str[100] = "Connecting...";
char playback_info_str[100] = "[--:--/--:--]";
char options_info_str[100] = "";

nlist *queue;
nlist *browser;

//Status bar elemens
progress_bar *vol_bar;
progress_bar *song_bar;
WINDOW* status_bar;

struct ui_state ui_current_state;

//convert seconds to mm:ss string
void format_time(int time,char *str){
	int seconds = time%60;
	int minutes = (time/60)%60;
	int hours = time/3600;
	
	if(hours == 0)
		sprintf(str,"%02i:%02i",minutes,seconds);
	else
		sprintf(str,"%i:%02i:%02i",hours,minutes,seconds);
}

void init_ui(){
	memset(&ui_current_state,0,sizeof(ui_current_state));
    //Starting color pairs
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_MAGENTA, COLOR_GREEN);
    init_pair(3, COLOR_GREEN, COLOR_MAGENTA);
    
    //Status bar
    status_bar = newwin(4,COLS,LINES-4,0);
	vol_bar = progress_bar_new(status_bar,0,COLS-38,20,"Volume");
	song_bar = progress_bar_new(status_bar,3,1,COLS-3,NULL);
    
    
    //The panels
    browser = nlist_new(LINES-3,COLS/2,0,0,NULL);
    queue = nlist_new(LINES-3,COLS/2+COLS%2,0,COLS/2,"Playlist");
}

void refresh_queue_data(){
	if(mpd_connection_get_error(conn_ui) != MPD_ERROR_SUCCESS) return;
	
	//To write clearly...
	linked_list** current = &(queue->items);
	mpd_song *song_ls;
	mpd_send_list_queue_meta(conn_ui);
	
	int index = 0;
	while((song_ls = mpd_recv_song(conn_ui)) != NULL){
		*current = (linked_list*) malloc(sizeof(linked_list));
		(*current)->next = NULL;
		sprintf(&(*current)->value[0],"%2i %s",++index,
			mpd_song_get_tag(song_ls,MPD_TAG_TITLE,0));
		current = &(*current)->next;
	}		
}

void refresh_ui_data(){
	if(mpd_connection_get_error(conn_ui) != MPD_ERROR_SUCCESS){
		strcpy(&playback_info_str[0],"[--:--/--:--]");
		strcpy(&options_info_str[0],"");
		progress_bar_set_value(song_bar,0);
		progress_bar_set_value(vol_bar,0);
		
		queue->items=NULL;
		browser->items=NULL;
		return;
	}
	
	mpd_status *sta = mpd_run_status(conn_ui);
	
	if(sta == NULL) return;
	
	ui_current_state.state = mpd_status_get_state(sta);
	int temp_queue_version = mpd_status_get_queue_version(sta);
	if(temp_queue_version != ui_current_state.last_queue_version
		|| ui_current_state.need_queue_update){
			
		refresh_queue_data();
			
		ui_current_state.last_queue_version = temp_queue_version;
		ui_current_state.need_queue_update = 0;
	}
	
	//Song info string ( Song Title - Song Artist )
	mpd_song *s = mpd_run_current_song(conn_ui);
	
	if(s != NULL){
		song_info_str[0] = '\0';
		
		if(mpd_song_get_tag(s,MPD_TAG_TITLE,0) != NULL)
			strcat(&song_info_str[0],
				mpd_song_get_tag(s,MPD_TAG_TITLE,0));
		
		strcat(&song_info_str[0]," - ");
		
		if(mpd_song_get_tag(s,MPD_TAG_ARTIST,0) != NULL)
			strcat(&song_info_str[0],
				mpd_song_get_tag(s,MPD_TAG_ARTIST,0));
	}else{
		strcpy(&song_info_str[0],"Nothing playing");
	}
	
	//Playback info string ([elapse/total])
	if(s != NULL){
		int elapsed = mpd_status_get_elapsed_time(sta);
		int total = mpd_status_get_total_time(sta);
		
		char elapsed_str[10];
		char total_str[10];
		char to_finish_str[10];
		
		format_time(elapsed,&elapsed_str[0]);
		format_time(total,&total_str[0]);
		format_time(total-elapsed,&to_finish_str[0]);
		
		sprintf(&playback_info_str[0],"[%s/%s] -%s",
			&elapsed_str[0],&total_str[0],&to_finish_str[0]);
		
		int percentage = 100*elapsed/total;
		progress_bar_set_value(song_bar,percentage);
		
	}else{
		strcpy(&playback_info_str[0],"[--:--/--:--]");
	}
	
	//Options info string (Repeat Random ....)
	sprintf(&options_info_str[0],
		"repeat:%i random:%i single:%i consume:%i",
		mpd_status_get_repeat(sta),
		mpd_status_get_random(sta),
		mpd_status_get_single(sta),
		mpd_status_get_consume(sta));
	
	//Volume bar
	progress_bar_set_value(vol_bar,mpd_status_get_volume(sta));
}

void refresh_ui(){
	//Order here its important, we want to see status bar over
	//the panels! So first draw panels
	
	nlist_draw(queue);
	nlist_draw(browser);
	
	wclear(status_bar);
	
	wborder(status_bar,
		ACS_VLINE,
		ACS_VLINE,
		ACS_HLINE,
		ACS_HLINE,
		ACS_LTEE,
		ACS_RTEE,
		ACS_LLCORNER,
		ACS_LRCORNER);
		
	progress_bar_draw(vol_bar);
	progress_bar_draw(song_bar);
	
	mvwprintw(status_bar,1,1,"%s",&song_info_str[0]);
	mvwprintw(status_bar,2,1,"%s",&playback_info_str[0]);
	mvwprintw(status_bar,2,30,"%s",&options_info_str[0]);
	
	wrefresh(status_bar);
}



    
/*    if(conn_ui != NULL){
		mpd_send_list_meta(conn_ui,"/");
		mpd_entity *ent;
		linked_list** current = &(browser->items);
		while((ent = mpd_recv_entity(conn_ui)) != NULL){
			*current = (linked_list*) malloc(sizeof(linked_list));
			(*current)->next = NULL;
			
			switch(mpd_entity_get_type(ent)){
				case MPD_ENTITY_TYPE_DIRECTORY:
					sprintf(&(*current)->value[0],"%s",mpd_directory_get_path(mpd_entity_get_directory(ent)));
					break;
				default:
					break;
			}
	
			current = &(*current)->next;
		}
	}
    
    return 0;
}*/

