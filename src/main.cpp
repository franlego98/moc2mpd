#include <iostream>
#include "ui.hpp"
#include "keyboard.hpp"
#include <mpd/client.h>
#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp> 


mpd_connection *conn_ui;
mpd_connection *conn_keyboard;
extern struct ui_state ui_current_state;
extern char song_info_str[100];

void init_mpd_clients(){
	
	conn_keyboard = mpd_connection_new(NULL,0,0);
	conn_ui = mpd_connection_new(NULL,0,0);
	
	if(mpd_connection_get_error(conn_keyboard) != MPD_ERROR_SUCCESS){
		strncpy(&song_info_str[0],
			mpd_connection_get_error_message(conn_keyboard),90);
	}

}

//Needs to be here due to ncurses and threading
void init_curses(){
	setlocale(LC_ALL, "");
	initscr();
	noecho();
	cbreak();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    curs_set(0);	
}

void ui_main(){
	while(1){
		refresh_ui_data();
		refresh_ui();
		if(mpd_connection_get_error(conn_ui) == MPD_ERROR_SUCCESS){
			
			if(ui_current_state.state == MPD_STATE_PLAY){
				boost::this_thread::sleep(
					boost::posix_time::milliseconds(1000));
			}else{
				int idle_result = mpd_run_idle(conn_ui);
				
				//check if it need to update something
				ui_current_state.need_queue_update 
					= (idle_result&MPD_IDLE_PLAYLIST)>0;
				
			}
		}else{
			boost::this_thread::sleep(
					boost::posix_time::milliseconds(2000));
			init_mpd_clients();
		}
	}
}

int main(){
	init_curses();
	init_ui();
	refresh_ui();
	
	init_mpd_clients();
	
	//thread for refresh ui data
	boost::thread ui_thread(ui_main);
	ui_thread.detach();
	
	//thread for handle keyboard
	boost::thread keyboard_thread(keyboard_main);
	keyboard_thread.join();
	
	return 0;
}
