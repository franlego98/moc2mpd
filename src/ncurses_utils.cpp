#include "ncurses_utils.hpp"

/*
 * 	Progress Bar
 */

progress_bar* progress_bar_new(WINDOW *w,int y,int x,int len,char* name){
	progress_bar *res = (progress_bar*) malloc(sizeof(progress_bar));
	memset(res,0x00,sizeof(progress_bar));
	
	if(w == NULL)
		res->win = stdscr;
	else
		res->win = w;
		
	res->name = name;
	res->value = 0;
	res->len = len;
	res->x = x;
	res->y = y;
	
	progress_bar_draw(res);
	return res;
}

void progress_bar_draw(progress_bar *bar){
	mvwaddch(bar->win,bar->y,bar->x,ACS_RTEE);
	
	int name_index = 0;
	int value_index = 0;
	wattron(bar->win,COLOR_PAIR(2));
	
	int percentage = bar->value*bar->len/100;
	
	char value_label[5];
	sprintf(&value_label[0],"%3i%%",bar->value);
	
	for(int i = 0;i < bar->len;i++){
		char toput = ' ';
		
		if(i > 0){
			if(i > bar->len-7 && value_label[value_index] != '\0'){
				toput = value_label[value_index++];
			}else if(bar->name != NULL){
				if(bar->name[name_index] != '\0')
					toput = bar->name[name_index++];
			}
		}
		if(i >= percentage){
			wattroff(bar->win,COLOR_PAIR(2));
			wattron(bar->win,COLOR_PAIR(3));
		}
		mvwaddch(bar->win,bar->y,bar->x+i+1,toput);
	}
	wattroff(bar->win,COLOR_PAIR(3));
	wattron(bar->win,COLOR_PAIR(1));
	
	mvwaddch(bar->win,bar->y,bar->x+bar->len,ACS_LTEE);
}

void progress_bar_set_value(progress_bar *bar,int value){
	if(value < 0){
		bar->value = 0;
	}else if(value > 100){
		bar->value = 100;
	}else{
		bar->value = value;
	}
}


/*
 * Ncurses list
 */

nlist *nlist_new(int y,int x,int height,int width,char *title){
	nlist *res = (nlist*) malloc(sizeof(nlist));
    res->win = newwin(y,x,height,width);
    res->items = NULL;
    res->title = title;
    res->selected = 0;
    
    return res;
}

void nlist_draw(nlist *l){
	wclear(l->win);
	
	wborder(l->win,
		ACS_VLINE,
		ACS_VLINE,
		ACS_HLINE,
		ACS_HLINE,
		ACS_ULCORNER,
		ACS_URCORNER,
		ACS_LLCORNER,
		ACS_LRCORNER);
	
	int n = 0;
	if(l->selected > getmaxy(l->win)-3)
		l->selected = getmaxy(l->win)-3;
	
	linked_list *current = l->items;
	while(current != NULL && n < getmaxy(l->win)-2){
		if(n == l->selected)
			wattron(l->win,COLOR_PAIR(2));
		
		mvwprintw(l->win,1+n,1,"%s",&current->value[0]);
		
		if(n == l->selected)
			wattroff(l->win,COLOR_PAIR(2));
			
		n++;
			
		current = current->next;
	}
		
	wrefresh(l->win);
}
