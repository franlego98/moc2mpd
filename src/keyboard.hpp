#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP
#include <curses.h>
#include <iostream>
#include <mpd/client.h>
#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp> 

#define KEY_QUIT			'q'
#define KEY_VOL_DOWN		','
#define KEY_VOL_UP			'.'
#define KEY_SEEK_FORWARD	KEY_RIGHT
#define KEY_SEEK_BACKWARD	KEY_LEFT
#define KEY_TOGGLE_PLAY		' '
#define KEY_LIST_DOWN		KEY_DOWN
#define KEY_LIST_UP			KEY_UP

void keyboard_main();

#endif
