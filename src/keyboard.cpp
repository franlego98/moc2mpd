#include "keyboard.hpp"

extern mpd_connection *conn_keyboard;

void keyboard_main(){
	while(1){
		int in = getch();
		
		if(in == KEY_QUIT){
			endwin();
			exit(0);
		}
		
		/*if(in == KEY_LIST_DOWN){
			browser->selected--;
			nlist_draw(browser);
		}
		
		if(in == KEY_LIST_UP){
			browser->selected++;
			nlist_draw(browser);
		}*/
		
		if(mpd_connection_get_error(conn_keyboard) == MPD_ERROR_SUCCESS){
			if(in == KEY_VOL_DOWN){
				mpd_run_change_volume(conn_keyboard,-2);
			}
			
			if(in == KEY_VOL_UP){
				mpd_run_change_volume(conn_keyboard,2);
			}
			
			if(in == KEY_SEEK_FORWARD){
				mpd_run_seek_current(conn_keyboard,1.f,1);

			}
			
			if(in == KEY_SEEK_BACKWARD){
				mpd_run_seek_current(conn_keyboard,-1.f,1);
			}
			
			if(in == KEY_TOGGLE_PLAY){
				mpd_run_toggle_pause(conn_keyboard);
			}
		}
	}
}
